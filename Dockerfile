
FROM python:3.9-slim
WORKDIR /app
COPY main.py /app/
COPY test.py /app/
CMD ["/bin/bash"]

# Comandos de ejecución
# docker build -t encontrar-par .
# docker run encontrar-par python main.py
# docker run encontrar-par python -m unittest test.py
