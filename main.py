def encontrar_par_que_suma(lista, suma_objetivo):
    left, right = 0, len(lista) - 1

    while left < right:
        suma_actual = lista[left] + lista[right]

        if suma_actual == suma_objetivo:
            return f"OK, matching pair ({lista[left]},{lista[right]})"
        elif suma_actual < suma_objetivo:
            left += 1
        else:
            right -= 1

    return "No"

print(encontrar_par_que_suma([2, 3, 6, 7], 9))
print(encontrar_par_que_suma([1, 5, 3, 7], 9))
