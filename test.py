import unittest
from main import encontrar_par_que_suma

class TestEncontrarParQueSuma(unittest.TestCase):

    def test_encontrar_par_existente(self):
        self.assertEqual(encontrar_par_que_suma([2, 3, 6, 8], 9), "OK, matching pair (3,6)")
        self.assertEqual(encontrar_par_que_suma([1, 2, 3, 4], 7), "OK, matching pair (3,4)")

    def test_encontrar_par_inexistente(self):
        self.assertEqual(encontrar_par_que_suma([1, 2, 3, 4], 10), "No")
        self.assertEqual(encontrar_par_que_suma([1, 5, 3, 7], 2), "No")

if __name__ == '__main__':
    unittest.main()
