# CI/CD

### Proyectos

### Proyecto 1: OV

# Descripción General
Creditos a personas de manera automática, evalúa a un prospecto en línea y se aprobaba o rechazaba al momento con las evaluaciones que se tienen a la mano

# Tecnologías y Herramientas Utilizadas
-Se hizo en una estructura de microservicios con js y python, usando expres y flask en el lado del back, y vue en el lado del front
-Se implementó websockets y rabbit para las evaluaciones, ya que estas no tenían un tiempo fijo de proceso.
-Se implementó scraping de datos para obtener data de webs

# Mi Rol en el Proyecto
Inicialmente como front, luego a dar mantenimiento a la parte back

# Desafíos y Aprendizajes
-Que todo funcione correctamente, el scraping de datos()
-Las evaluaciones, implementación de politicas, la estructura de la data

# Aprendizajes clave o habilidades desarrolladas a través del proyecto.
Implementación de colas, comunicación en tiempo real(streaming) 

# Código 
El proyecto es tan grande que resumirlo en unas líneas es una injusticia


--------------------------------------------

### Proyecto 2: SCRAPING DIARIOS

# Descripción General
Se desea scrapear data de los principales diarios peruanos y chilenos

# Tecnologías y Herramientas Utilizadas
-Se hizo en python, selenium, bs
-Aws, docker, ECS, para el despliegue y la programación de tareas

# Mi Rol en el Proyecto
-Owner completo, el proyecto lo hice de 0

# Desafíos y Aprendizajes
-Creación del dockerfile, esto debido a que se tenía que descargar un chrome y su controlador en el dockerfile, pero estas tienen que ser la misma versión, caso contrario fallan en su integración

# Aprendizajes clave o habilidades desarrolladas a través del proyecto.
Comandos linux, aws, dynamo


# Código dockerfile
FROM python:3.10

COPY . /app
WORKDIR /app

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable

RUN apt-get update && apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver-linux64.zip https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/119.0.6045.105/linux64/chromedriver-linux64.zip
RUN unzip /tmp/chromedriver-linux64.zip -d /tmp/
RUN mv /tmp/chromedriver-linux64/chromedriver /usr/local/bin/


ENV DISPLAY=:99

RUN pip install -r requirements.txt

RUN python -m spacy download es_core_news_sm

CMD ["python", "./main.py"]