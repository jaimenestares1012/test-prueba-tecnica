# CI/CD

### CI/CD

Dentro de la integración y despliegue continuo he trabajado con los pipelines de gitlab, estos han sido configurados de tal forma que cada que se haga un commit, esté vinvulado a un .gitlab-ci.yml, el cual hace automatiza el despligue sea cual sea el stage




# .gitlab-ci.yml

image: tiangolo/docker-with-compose

before_script:

- docker login registry.test.com --username=test -p=test

variables:
PORTDOCKER: 5000
PORTSALIDA: 5000
IMAGENAME: sc-test-py
DOCKER_STACK: sc-test-py

stages:

- build
- deploy-to-dev
- build-to-pro

build:
stage: build
tags: - build
only: - develop
variables:
ENVDEPLOY: development
script: - docker-compose build - docker-compose push

deploy-to-dev:
stage: deploy-to-dev
only: - develop
tags: - swarm-dev
variables:
ENVDEPLOY: dev
NODO: dev
script: - docker stack deploy -c docker-compose.yml $DOCKER_STACK --with-registry-auth

build-to-pro:
stage: build-to-pro
only: - master
tags: - build
variables:
IMAGENAME: sc-sunarp-py
ENVDEPLOY: pro
script: - docker build -t ${IMAGENAME} .
    - docker tag ${IMAGENAME} registry.test.com/${IMAGENAME}:${CI_PIPELINE_ID}
    - docker push registry.test.com/${IMAGENAME}:${CI_PIPELINE_ID}

# docker-compose.yml

version: "3"
services:
app:
image: ${DMINIO}/${IMAGENAME}:${CI_PIPELINE_ID}
build: .
command: python -u main.py ${ENVDEPLOY}
ports: - "5000:5000"
restart: always
deploy:
mode: replicated
replicas: 1
placement:
constraints: - node.labels.name == dev
logging:
driver: gelf
options:
tag: ${IMAGENAME}
volumes: - /etc/timezone:/etc/timezone:ro - /etc/localtime:/etc/localtime:ro
