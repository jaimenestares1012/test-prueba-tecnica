# Documentación e Investigación

## RTFM y LMGTFY

### RTFM ("Read The 'Fine' Manual")
La última vez que apliqué la técnica RTFM fue mientras trabajaba en un proyecto utilizando Flask, un microframework de Python. Me enfrenté a un desafío específico relacionado con la implementación de variables de entorno para configurar la aplicación en diferentes entornos de desarrollo.

### LMGTFY ("Let Me Google That For You")
Recientemente, mientras desarrollaba una aplicación web con Vue.js, necesitaba implementar una característica de autocompletado en un campo de búsqueda. No estaba seguro de cuál era la mejor biblioteca para integrar con Vue.js que ofreciera esta funcionalidad

## Sistema Operativo y Lenguajes de Programación

### Sistema Operativo
Actualmente, mi sistema operativo principal es Windowns 11, para desarollar es una de las más completas en el sentido de que todo es más fácil y compatible.



### Lenguajes de Programación y Tecnologías
- **Python**: Tengo experiencia avanzada en Python, con conocimientos en bibliotecas y frameworks como Flask, Selenium, y Pandas. Utilizo Python principalmente para desarrollo de microservicios, scraping y análisis de datos.
- **JavaScript**: Dominio del lenguaje JavaScript, con experiencia en Node.js (back), Vue.js (front), y el manejo de tecnologías de tiempo real como WebSockets.
- **Tecnologías adicionales**: He trabajado con LoopBack para la creación de APIs REST(poco), RabbitMQ para la gestión de colas de mensajes en sistemas distribuidos, y he implementado comunicaciones en tiempo real con Sockets.

---

Este archivo resume mi experiencia reciente con RTFM y LMGTFY, así como una visión general de las tecnologías y el sistema operativo que utilizo. Estoy constantemente buscando aprender y adaptarme a nuevas herramientas y lenguajes para mejorar mi eficiencia
